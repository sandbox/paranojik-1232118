
OPENID PROVIDER AX
------------------

This module provides an API for exposing user related information as OpenID
fields according to the OpenID AX specification 
http://openid.net/specs/openid-attribute-exchange-1_0.html

Typically, OpenID Provider AX is used together with OpenID Content Profile Field 
or OpenID Provider Persona to expose user profile information as OpenID AX 
fields.

http://drupal.org/project/openid_cp_field
http://drupal.org/project/openid_provider_persona